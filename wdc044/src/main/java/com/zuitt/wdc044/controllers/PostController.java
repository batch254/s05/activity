package com.zuitt.wdc044.controllers;

import com.zuitt.wdc044.models.Post;
import com.zuitt.wdc044.services.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
public class PostController {

    @Autowired
    PostService postService;

    @RequestMapping(value = "/posts", method = RequestMethod.POST)
    public ResponseEntity<Object> createPost(@RequestHeader(value = "Authorization") String stringToken, @RequestBody Post post){

        postService.createPost(stringToken, post);
        return new ResponseEntity<>("Post created successfully", HttpStatus.CREATED);
    }


    @RequestMapping(value = "/posts", method = RequestMethod.GET)
    public ResponseEntity<Iterable<Post>>getPosts(){

        Iterable<Post> posts =  postService.getPosts();
        return ResponseEntity.ok().body(posts);
    }

    //Edit a post
    @RequestMapping(value = "/posts/{postId}", method = RequestMethod.PUT)
        public ResponseEntity<Object> updatePost(@PathVariable Long postId, @RequestHeader (value = "Authorization") String stringToken, @RequestBody Post post ){
        return postService.updatePost(postId, stringToken, post);
    }

    //Delete a post
    @RequestMapping(value = "/posts/{postId}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> deletePost(@PathVariable Long postId, @RequestHeader (value = "Authorization") String stringToken){
        return postService.deletePost(postId, stringToken);
    }

    @RequestMapping(value = "/myPosts", method = RequestMethod.GET)
    public ResponseEntity<Object> getPost(@RequestHeader (value = "Authorization") String stringToken){
        return new ResponseEntity<>(postService.getPost(stringToken), HttpStatus.OK);
    }

}
